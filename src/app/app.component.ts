import { Component, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  toolName = "ngx-translate"
  inputValue = "";
  localeList = [
    {name: "English", code: "en"},
    {name: "Malayalam", code: "ml"}
  ];
  currentLocale = "en";
  changeLangText = "Change the language below"

  translateSubscription$: Subscription;

  constructor(private translate: TranslateService) {

    // localizae field in a .ts file
   this.translateSubscription$ =  this.translate.stream(["CHANGELANGUAGE"]).subscribe(data => {
    if (data) {
      this.changeLangText = data["CHANGELANGUAGE"]["CHANGE"];
    }
  })
  }

  changeLocale(locale: string) {
    this.currentLocale = locale
    this.translate.use(this.currentLocale);
  }

  ngOnDestroy() {
    if(this.translateSubscription$) {
      this.translateSubscription$.unsubscribe();
    }
  }
}
